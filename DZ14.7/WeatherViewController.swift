
import UIKit

class WeatherViewController: UIViewController {
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var refreshingActivityIndicator: UIActivityIndicatorView!
    
    var weather = CityWeather()
    var weatherForecast: [WeatherForecast] = []
    var isFirst = true
    let city = "Moscow"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        if Persistance().isFirstValue == nil{
            Persistance().isFirst = isFirst
        }
        else{
            isFirst = Persistance().isFirst
        }
        if isFirst{
            WeatherLoader().LoadWeatherForecast(fromCity: city) { weatherForecast in
                self.weatherForecast = weatherForecast
                self.tableView.reloadData()
                self.refreshingActivityIndicator.stopAnimating()
                WeatherLoader().LoadWeather(fromCity: self.city){ weather in
                    self.weather = weather
                    self.tempLabel.text = "\(weather.temp) °C"
                    self.cityLabel.text = weather.name
                }
                self.saveWeather()
            }
            Persistance().isFirst = false
            isFirst = false
        }
        else{
            readWeather()
            WeatherLoader().LoadWeatherForecast(fromCity: city) { weatherForecast in
                self.weatherForecast = weatherForecast
                self.tableView.reloadData()
                self.refreshingActivityIndicator.stopAnimating()
                WeatherLoader().LoadWeather(fromCity: self.city){ weather in
                    self.weather = weather
                    self.tempLabel.text = "\(weather.temp) °C"
                    self.cityLabel.text = weather.name
                }
                self.saveWeather()
            }
        }
//        Persistance().isFirst = nil     // Clear for first start or change city
    }
    private func saveWeather(){
        Persistance().saveWeather(cityWeather: weather)
        Persistance().saveForecast(foreCast: weatherForecast)
    }
    private func readWeather(){
        weather = Persistance().readWeather()
        self.tempLabel.text = "\(weather.temp) °C"
        self.cityLabel.text = weather.name
        weatherForecast = Persistance().readForecast()
        self.tableView.reloadData()
    }
}
extension WeatherViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherForecast.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = weatherForecast[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherTableViewCell
        cell.dateLabel.text = model.date
        cell.tempLabel.text = model.temp
        return cell
    }
}
