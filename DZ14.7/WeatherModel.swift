
import Foundation

struct CityWeather {
    var name: String
    var temp: String
    init(){
        temp = ""
        name =  ""
    }
}
struct WeatherForecast {
    var date: String
    var temp: String
}

//struct WeatherModewl{
//    let date: String
//    let temperature: String
//}
//Weather

struct Temperature: Codable {
    let main: Main
    let name: String
}
struct Main: Codable {
    let temp: Double
}


//WeatherForecast

struct TemperatureForecast: Codable {
    let list: [List]
}
struct List: Codable{
    let main: MainForecast
    let date: String
    
    enum CodingKeys: String, CodingKey{
        case main
        case date = "dt_txt"
    }
}
struct MainForecast: Codable{
    let temp: Double
}
