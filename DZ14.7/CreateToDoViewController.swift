
import UIKit

class CreateToDoViewController: UIViewController {

    var numberOfRow = Int()
    var isNew: Bool = true
    var isChange: Bool = false
    var isRealm = Bool() // true: Realm, false: CoreData
    
    var object = ModelToDo(date: "", name: "", description: "")
    
    let nameLabel = UILabel()
    let nameField = UITextField()
    let dateLabel = UILabel()
    let dateField = UITextField()
    let descriptionLabel = UILabel()
    let descriptionField = UITextField()
    let changeButton = UIButton()
    let saveButton = UIButton()
    let backButton = UIButton()
    
    let nameFrame = CGRect(x: 16, y: 96, width: 100, height: 34)
    let dateFrame = CGRect(x: 16, y: 163, width: 100, height: 34)
    let descriptionFrame = CGRect(x: 16, y: 230, width: 340, height: 34)
    let changeButtonFrame = CGRect(x: 16, y: 280, width: 100, height: 34)
    let backButtonFrame = CGRect(x: 150, y: 280, width: 100, height: 34)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isNew{
            addNewToDoToView()
        }
        else{
            showToDoOnView()
        }
        // Do any additional setup after loading the view.
    }
    
    func addNewToDoToView(){
        for view in view.subviews {
            view.removeFromSuperview()
        }
        nameField.frame = nameFrame
        nameField.placeholder = "Название задачи"
        dateField.frame = dateFrame
        dateField.placeholder = "Дата"
        descriptionField.frame = descriptionFrame
        descriptionField.placeholder = "Описание задачи"
        saveButton.frame = changeButtonFrame
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.setTitleColor(.blue, for: .normal)
        saveButton.layer.borderColor = UIColor.blue.cgColor
        saveButton.layer.borderWidth = 1
        saveButton.layer.cornerRadius = 5
        backButton.frame = backButtonFrame
        backButton.setTitle("Назад", for: .normal)
        backButton.setTitleColor(.blue, for: .normal)
        backButton.layer.borderColor = UIColor.blue.cgColor
        backButton.layer.borderWidth = 1
        backButton.layer.cornerRadius = 5
        
        for view in [nameField, dateField, descriptionField, saveButton, backButton]{
            self.view.addSubview(view)
        }
        if isChange{
            nameField.text = object.name
            dateField.text = object.date
            descriptionField.text = object.description
        }
        saveButton.addTarget(self, action: #selector(pressSave), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(pressBack), for: .touchUpInside)
    }
    
    func showToDoOnView(){
        for view in view.subviews {
            view.removeFromSuperview()
        }
        nameLabel.frame = nameFrame
        dateLabel.frame = dateFrame
        descriptionLabel.frame = descriptionFrame
        changeButton.frame = changeButtonFrame
        changeButton.setTitle("Изменить", for: .normal)
        changeButton.setTitleColor(.blue, for: .normal)
        changeButton.layer.borderColor = UIColor.blue.cgColor
        changeButton.layer.borderWidth = 1
        changeButton.layer.cornerRadius = 5
        backButton.frame = backButtonFrame
        backButton.setTitle("Назад", for: .normal)
        backButton.setTitleColor(.blue, for: .normal)
        backButton.layer.borderColor = UIColor.blue.cgColor
        backButton.layer.borderWidth = 1
        backButton.layer.cornerRadius = 5
        
        
        if isRealm{
            let model = Persistance().readToDoTroughRealm(index: numberOfRow)
            nameLabel.text = model.name
            dateLabel.text = model.date
            descriptionLabel.text = model.description
        }
        else{
            let model = Persistance().readToDoTroughCoreData(index: numberOfRow)
            nameLabel.text = model.name
            dateLabel.text = model.date
            descriptionLabel.text = model.description
        }
        
        for view in [nameLabel, dateLabel, descriptionLabel, changeButton, backButton]{
            self.view.addSubview(view)
        }
        changeButton.addTarget(self, action: #selector(pressChange), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(pressBack), for: .touchUpInside)

    }
    
    @objc func pressChange(){
        object.date = dateLabel.text ?? ""
        object.name = nameLabel.text ?? ""
        object.description = descriptionLabel.text ?? ""
        isChange = true
        addNewToDoToView()
    }
    
    @objc func pressBack(){
    dismiss(animated: true, completion: nil)
    }
    
    @objc func pressSave(){
        if isRealm{
            if isChange{
                Persistance().deleteToDoTroughRealm(index: numberOfRow)
                isChange = false
            }
                if dateField.text != "" || nameField.text != "" || descriptionField.text != ""{
                Persistance().addToDoTroughRealm(todo: ModelToDo(date: dateField.text ?? "", name: nameField.text ?? "", description: descriptionField.text ?? ""))
                    dismiss(animated: true, completion: nil)
                }
                else { return }
        }
        else{
            if isChange{
                Persistance().deleteToDoTroughCoreData(index: numberOfRow)
                isChange = false
            }
                if dateField.text != "" || nameField.text != "" || descriptionField.text != ""{
                Persistance().addToDoTroughCoreData(todo: ModelToDo(date: dateField.text ?? "", name: nameField.text ?? "", description: descriptionField.text ?? ""))
                    dismiss(animated: true, completion: nil)
                }
                else { return }
        }
    }
}
