
import Foundation

struct ModelToDo{
    var date: String
    var name: String
    var description: String
}
