
import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBAction func saveButton(_ sender: Any) {
        if nameTextField.text != "" && surnameTextField.text != ""{
            let name = nameTextField.text!.trimmingCharacters(in: .whitespaces)
            let surname = surnameTextField.text!.trimmingCharacters(in: .whitespaces)
            Persistance.shared.userName = "\(name) \(surname)"
            helloLabel.text = "Привет, \(name) \(surname)!"
        }
        else{
            Persistance().remove()
            helloLabel.text = "Введите свои данные"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Persistance.shared.value != nil{
            if let name = Persistance.shared.userName{
                helloLabel.text = "Привет, \(name)!"
            }
        }
        else{
            helloLabel.text = "Введите свои данные"
        }
    }
}

