
import Foundation
import UIKit
import Alamofire

class WeatherLoader{
   
    func LoadWeather(fromCity city: String, completion: @escaping(CityWeather) -> Void){
        var temperature = CityWeather()
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(APIKey)")!
        let request = URLRequest(url: url)
        AF.request(request).responseJSON {response in
            let json = response.value as! NSDictionary
            if let main = json["main"] as? NSDictionary{
                if let name = json["name"]{
                    if let temp = main["temp"]{
                        temperature.name = "\(name)"
                        temperature.temp = "\(temp)"
                    }
                }
            }
                completion(temperature)
        }
    }
    
    func LoadWeatherForecast(fromCity city: String, completion: @escaping ([WeatherForecast]) -> Void){
        let url = URL(string: "https://api.openweathermap.org/data/2.5/forecast?q=\(city),RU&units=metric&appid=\(APIKey)")!
        let request = URLRequest(url: url)
        var forecast: [WeatherForecast] = []
        AF.request(request).responseJSON { response in
            let json = response.value as! NSDictionary
            if let list = json["list"] as? [NSDictionary]{
                for data in list{
                    if let dates = data["dt_txt"] as? String{
                        if let main = data["main"] as? NSDictionary{
                            if let temp = main["temp"] {
                                let object = WeatherForecast(date: dates, temp: "\(temp)")
                                forecast.append(object)
                            }
                        }
                    }
                }
                completion(forecast)
            }
        }
    }
}

