import Foundation
import RealmSwift
import CoreData

class ToDo: Object{
    @objc dynamic var name = ""
    @objc dynamic var date = ""
    @objc dynamic var descript = ""
}
class Weather: Object{
    @objc dynamic var temp = ""
    @objc dynamic var city = ""
}
class Forecast: Object{
    @objc dynamic var date = ""
    @objc dynamic var temp = ""
}
class Persistance{
    static let shared = Persistance()
    
    private let realmCoredata = "Persistance.realmCoredata"
    private let userNameKey = "Persistance.userNameKey"
    private let isFirstKey = "Persistance.idFirst"
    private let realm = try! Realm()
    
    var rCDValue: Any? {
        get {return UserDefaults.standard.value(forKey: realmCoredata)}
    }
    var value: Any? {
        get {
            return UserDefaults.standard.value(forKey: userNameKey)
        }
    }
    var isFirstValue: Any? {
        get {
            return UserDefaults.standard.value(forKey: isFirstKey)
        }
    }
    var rCD: Bool! {
        set { UserDefaults.standard.set(newValue, forKey: realmCoredata)}
        get { return UserDefaults.standard.bool(forKey: realmCoredata)}
    }
    var isFirst: Bool! {
        set { UserDefaults.standard.set(newValue, forKey: isFirstKey) }
        get { return UserDefaults.standard.bool(forKey: isFirstKey) }
    }
    var userName: String! {
        set { UserDefaults.standard.set(newValue, forKey: userNameKey) }
        get { return UserDefaults.standard.string(forKey: userNameKey) }
    }
    func remove(){
        UserDefaults.standard.removeObject(forKey: userNameKey)
    }
    func addToDoTroughRealm(todo: ModelToDo){
        let toDo = ToDo()
        toDo.date = todo.date
        toDo.name = todo.name
        toDo.descript = todo.description
        try! realm.write{
            realm.add(toDo)
        }
    }
    func addToDoTroughCoreData(todo: ModelToDo){
        let context = getContext()
        guard let entity = NSEntityDescription.entity(forEntityName: "ToDoCD", in: context)
            else { return }
        let taskObject = ToDoCD(entity: entity, insertInto: context)
        taskObject.date = todo.date
        taskObject.name = todo.name
        taskObject.descript = todo.description
        
        do{
            try context.save()
        } catch let error as NSError{
            print(error.localizedDescription)
        }
    }
    func readToDoTroughRealm(index: Int) -> ModelToDo{
        let allToDo = realm.objects(ToDo.self)
        let object = allToDo[index]
        let todo = ModelToDo(date: object.date, name: object.name, description: object.descript)
        return todo
    }
    func readToDoTroughCoreData(index: Int) -> ModelToDo{
        var todo = ModelToDo(date: "", name: "", description: "")
        let context = getContext()
        let fetchRequest: NSFetchRequest<ToDoCD> = ToDoCD.fetchRequest()
        if let objects = try? context.fetch(fetchRequest){
            let object = objects[index]
            todo.date = object.date ?? ""
            todo.name = object.name ?? ""
            todo.description = object.descript ?? ""
        }
        return todo
    }
    func readAllToDoTroughRealm() -> [ModelToDo]{
        let allTodo = realm.objects(ToDo.self)
        var allToDo: [ModelToDo] = []
        for todo in allTodo{
            allToDo.append(ModelToDo(date: todo.date, name: todo.name, description: todo.descript))
        }
        return allToDo
    }
    func readAllToDoTroughCoreData() -> [ModelToDo]{
        var task: [ToDoCD] = []
        var todo = [ModelToDo]()
        let context = getContext()
        let fetchRequest: NSFetchRequest<ToDoCD> = ToDoCD.fetchRequest()
        do{
            task = try context.fetch(fetchRequest)
            for item in task{
                let tasK = ModelToDo(date: item.date ?? "", name: item.name ?? "", description: item.descript ?? "")
                todo.append(tasK)
            }
        } catch let error as NSError{
            print(error.localizedDescription)
        }
        return todo
    }
    func deleteToDoTroughRealm(index: Int){
        let allToDo = realm.objects(ToDo.self)
        let object = allToDo[index]
        try! realm.write{
            realm.delete(object)
        }
    }
    func deleteToDoTroughCoreData(index: Int){
        let context = getContext()
        let fetchRequest: NSFetchRequest<ToDoCD> = ToDoCD.fetchRequest()
        if let objects = try? context.fetch(fetchRequest){
            let object = objects[index]
            context.delete(object)
        }
        do{
            try context.save()
        } catch let error as NSError{
            print(error.localizedDescription)
        }
    }
    func saveWeather(cityWeather: CityWeather){
        var objects = realm.objects(Weather.self)
        let weather = Weather()
        try! realm.write{
            for object in objects{
                realm.delete(object)
            }
        }
        weather.city = cityWeather.name
        weather.temp = cityWeather.temp
        try! realm.write{
            realm.add(weather)
        }
        objects = realm.objects(Weather.self)
    }
    func readWeather() -> CityWeather{
        let weather = realm.objects(Weather.self)
        let object = weather[0]
        var cityWeather = CityWeather()
        cityWeather.name = object.city
        cityWeather.temp = object.temp
        return cityWeather
    }
    func saveForecast(foreCast: [WeatherForecast]){
        let objects = realm.objects(Forecast.self)
        try! realm.write{
            for object in objects{
                realm.delete(object)
            }
        }
        for date in foreCast{
            let temperature = Forecast()
            temperature.date = date.date
            temperature.temp = date.temp
            try! realm.write{
                realm.add(temperature)
            }
        }
    }
    func readForecast() -> [WeatherForecast]{
        let objects = realm.objects(Forecast.self)
        var weatherForecast: [WeatherForecast] = []
        for object in objects{
            let obj = WeatherForecast(date: object.date, temp: object.temp)
            weatherForecast.append(obj)
        }
        return weatherForecast
    }
    private func getContext() -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        return context
    }
}
