
import UIKit

class ViewControllerOne: UIViewController {
    var toDos: [ModelToDo] = []
    var pressedCell = Int()

    @IBOutlet weak var realmCoreData: UISegmentedControl!
    @IBAction func realmCoreDataSwitch(_ sender: Any) {
        if realmCoreData.selectedSegmentIndex == 0{
            Persistance.shared.rCD = true
            toDos = Persistance().readAllToDoTroughRealm()
            tableView.reloadData()
        }
        else{
            Persistance.shared.rCD = false
            toDos = Persistance().readAllToDoTroughCoreData()
            tableView.reloadData()
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        print("\(Persistance().readAllToDoTroughCoreData())")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
                if Persistance.shared.rCDValue == nil{
            Persistance.shared.rCD = true
            realmCoreData.selectedSegmentIndex = 0
        }
        if Persistance.shared.rCD{
            realmCoreData.selectedSegmentIndex = 0
            toDos = Persistance().readAllToDoTroughRealm()
            tableView.reloadData()
        }
        else{
            realmCoreData.selectedSegmentIndex = 1
            toDos = Persistance().readAllToDoTroughCoreData()
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell, let index = tableView.indexPath(for: cell){
            if let vc = segue.destination as? CreateToDoViewController, segue.identifier == "New ToDo"{
                vc.numberOfRow = index.row
                vc.isNew = false
                vc.isRealm = isRealm()
            }
        }
        if let button = sender as? UIButton{
            if let vc = segue.destination as? CreateToDoViewController, segue.identifier == "AddNewToDo"{
                                vc.isNew = true
                                vc.isRealm = isRealm()
            }
        }
    }
    @objc func pressDelete(sender: UIButton){
        if realmCoreData.selectedSegmentIndex == 0{
            Persistance().deleteToDoTroughRealm(index: sender.tag)
            toDos = Persistance().readAllToDoTroughRealm()
            tableView.reloadData()
        }
        else{
            Persistance().deleteToDoTroughCoreData(index: sender.tag)
            toDos = Persistance().readAllToDoTroughCoreData()
            tableView.reloadData()
        }

    }
    func isRealm() -> Bool{
        var bool = Bool()
        if realmCoreData.selectedSegmentIndex == 0{
            bool = true
        }
        else{
            bool = false
        }
        return bool
    }
}
extension ViewControllerOne: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model: ModelToDo = toDos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TableViewCell
        cell.dateLabel.text = model.date
        cell.nameLabel.text = model.name
        cell.descriptionLabel.text = model.description
        cell.deleteToDo.tag = indexPath.row
        cell.deleteToDo.addTarget(self, action: #selector(pressDelete), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        pressedCell = indexPath.row
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
